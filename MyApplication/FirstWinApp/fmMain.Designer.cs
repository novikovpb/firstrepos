﻿
namespace FirstWinApp
{
    partial class fmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fmMain));
            this.labelHowToDo = new System.Windows.Forms.Label();
            this.cmbBoxStopBits = new System.Windows.Forms.ComboBox();
            this.cmbBoxParity = new System.Windows.Forms.ComboBox();
            this.cmbBoxDataBit = new System.Windows.Forms.ComboBox();
            this.cmbBoxBaudrate = new System.Windows.Forms.ComboBox();
            this.cmbBoxPorts = new System.Windows.Forms.ComboBox();
            this.btnStartAndStop = new System.Windows.Forms.Button();
            this.labelStopBits = new System.Windows.Forms.Label();
            this.labelParity = new System.Windows.Forms.Label();
            this.labelDataBit = new System.Windows.Forms.Label();
            this.labelBaudrate = new System.Windows.Forms.Label();
            this.labelPort = new System.Windows.Forms.Label();
            this.radioBtnBin = new System.Windows.Forms.RadioButton();
            this.radioBtnHex = new System.Windows.Forms.RadioButton();
            this.btnClear = new System.Windows.Forms.Button();
            this.gvData = new System.Windows.Forms.DataGridView();
            this.columnTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRefreshPorts = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).BeginInit();
            this.SuspendLayout();
            // 
            // labelHowToDo
            // 
            this.labelHowToDo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHowToDo.AutoSize = true;
            this.labelHowToDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelHowToDo.Location = new System.Drawing.Point(12, 57);
            this.labelHowToDo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelHowToDo.Name = "labelHowToDo";
            this.labelHowToDo.Size = new System.Drawing.Size(250, 51);
            this.labelHowToDo.TabIndex = 10;
            this.labelHowToDo.Text = "Для корректной работы программы \r\nтребуется выставить настройки, \r\nаналогичные на" +
    "стройкам УФС-232\r\n";
            this.labelHowToDo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmbBoxStopBits
            // 
            this.cmbBoxStopBits.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.cmbBoxStopBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbBoxStopBits.FormattingEnabled = true;
            this.cmbBoxStopBits.Location = new System.Drawing.Point(88, 221);
            this.cmbBoxStopBits.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbBoxStopBits.Name = "cmbBoxStopBits";
            this.cmbBoxStopBits.Size = new System.Drawing.Size(174, 24);
            this.cmbBoxStopBits.TabIndex = 4;
            // 
            // cmbBoxParity
            // 
            this.cmbBoxParity.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.cmbBoxParity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbBoxParity.FormattingEnabled = true;
            this.cmbBoxParity.Location = new System.Drawing.Point(88, 189);
            this.cmbBoxParity.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbBoxParity.Name = "cmbBoxParity";
            this.cmbBoxParity.Size = new System.Drawing.Size(174, 24);
            this.cmbBoxParity.TabIndex = 3;
            // 
            // cmbBoxDataBit
            // 
            this.cmbBoxDataBit.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.cmbBoxDataBit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbBoxDataBit.FormattingEnabled = true;
            this.cmbBoxDataBit.Location = new System.Drawing.Point(88, 156);
            this.cmbBoxDataBit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbBoxDataBit.Name = "cmbBoxDataBit";
            this.cmbBoxDataBit.Size = new System.Drawing.Size(174, 24);
            this.cmbBoxDataBit.TabIndex = 2;
            // 
            // cmbBoxBaudrate
            // 
            this.cmbBoxBaudrate.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.cmbBoxBaudrate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbBoxBaudrate.FormattingEnabled = true;
            this.cmbBoxBaudrate.Location = new System.Drawing.Point(88, 124);
            this.cmbBoxBaudrate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbBoxBaudrate.Name = "cmbBoxBaudrate";
            this.cmbBoxBaudrate.Size = new System.Drawing.Size(174, 24);
            this.cmbBoxBaudrate.TabIndex = 1;
            // 
            // cmbBoxPorts
            // 
            this.cmbBoxPorts.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.cmbBoxPorts.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbBoxPorts.FormattingEnabled = true;
            this.cmbBoxPorts.Location = new System.Drawing.Point(88, 14);
            this.cmbBoxPorts.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbBoxPorts.Name = "cmbBoxPorts";
            this.cmbBoxPorts.Size = new System.Drawing.Size(142, 24);
            this.cmbBoxPorts.TabIndex = 0;
            // 
            // btnStartAndStop
            // 
            this.btnStartAndStop.BackColor = System.Drawing.Color.SeaGreen;
            this.btnStartAndStop.Location = new System.Drawing.Point(15, 260);
            this.btnStartAndStop.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnStartAndStop.Name = "btnStartAndStop";
            this.btnStartAndStop.Size = new System.Drawing.Size(247, 36);
            this.btnStartAndStop.TabIndex = 5;
            this.btnStartAndStop.Text = "Старт";
            this.btnStartAndStop.UseVisualStyleBackColor = false;
            this.btnStartAndStop.Click += new System.EventHandler(this.btnStartAndStop_Click);
            // 
            // labelStopBits
            // 
            this.labelStopBits.AutoSize = true;
            this.labelStopBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelStopBits.Location = new System.Drawing.Point(12, 224);
            this.labelStopBits.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelStopBits.Name = "labelStopBits";
            this.labelStopBits.Size = new System.Drawing.Size(68, 17);
            this.labelStopBits.TabIndex = 4;
            this.labelStopBits.Text = "Stop Bits:";
            // 
            // labelParity
            // 
            this.labelParity.AutoSize = true;
            this.labelParity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelParity.Location = new System.Drawing.Point(12, 192);
            this.labelParity.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelParity.Name = "labelParity";
            this.labelParity.Size = new System.Drawing.Size(48, 17);
            this.labelParity.TabIndex = 3;
            this.labelParity.Text = "Parity:";
            // 
            // labelDataBit
            // 
            this.labelDataBit.AutoSize = true;
            this.labelDataBit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDataBit.Location = new System.Drawing.Point(12, 159);
            this.labelDataBit.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelDataBit.Name = "labelDataBit";
            this.labelDataBit.Size = new System.Drawing.Size(62, 17);
            this.labelDataBit.TabIndex = 2;
            this.labelDataBit.Text = "Data Bit:";
            // 
            // labelBaudrate
            // 
            this.labelBaudrate.AutoSize = true;
            this.labelBaudrate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelBaudrate.Location = new System.Drawing.Point(10, 127);
            this.labelBaudrate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelBaudrate.Name = "labelBaudrate";
            this.labelBaudrate.Size = new System.Drawing.Size(70, 17);
            this.labelBaudrate.TabIndex = 1;
            this.labelBaudrate.Text = "Baudrate:";
            // 
            // labelPort
            // 
            this.labelPort.AutoSize = true;
            this.labelPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPort.Location = new System.Drawing.Point(10, 18);
            this.labelPort.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPort.Name = "labelPort";
            this.labelPort.Size = new System.Drawing.Size(45, 17);
            this.labelPort.TabIndex = 9;
            this.labelPort.Text = "Порт:";
            // 
            // radioBtnBin
            // 
            this.radioBtnBin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radioBtnBin.AutoSize = true;
            this.radioBtnBin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioBtnBin.Location = new System.Drawing.Point(628, 422);
            this.radioBtnBin.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioBtnBin.Name = "radioBtnBin";
            this.radioBtnBin.Size = new System.Drawing.Size(48, 21);
            this.radioBtnBin.TabIndex = 3;
            this.radioBtnBin.Text = "BIN";
            this.radioBtnBin.UseVisualStyleBackColor = true;
            // 
            // radioBtnHex
            // 
            this.radioBtnHex.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radioBtnHex.AutoSize = true;
            this.radioBtnHex.Checked = true;
            this.radioBtnHex.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radioBtnHex.Location = new System.Drawing.Point(558, 422);
            this.radioBtnHex.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioBtnHex.Name = "radioBtnHex";
            this.radioBtnHex.Size = new System.Drawing.Size(54, 21);
            this.radioBtnHex.TabIndex = 2;
            this.radioBtnHex.TabStop = true;
            this.radioBtnHex.Text = "HEX";
            this.radioBtnHex.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClear.Location = new System.Drawing.Point(275, 414);
            this.btnClear.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(139, 36);
            this.btnClear.TabIndex = 1;
            this.btnClear.Text = "Очистить";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // gvData
            // 
            this.gvData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnTime,
            this.columnData});
            this.gvData.Location = new System.Drawing.Point(275, 14);
            this.gvData.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gvData.Name = "gvData";
            this.gvData.RowHeadersWidth = 51;
            this.gvData.RowTemplate.Height = 24;
            this.gvData.Size = new System.Drawing.Size(401, 385);
            this.gvData.TabIndex = 0;
            // 
            // columnTime
            // 
            this.columnTime.HeaderText = "Time";
            this.columnTime.MinimumWidth = 6;
            this.columnTime.Name = "columnTime";
            this.columnTime.Width = 125;
            // 
            // columnData
            // 
            this.columnData.HeaderText = "Data";
            this.columnData.MinimumWidth = 6;
            this.columnData.Name = "columnData";
            this.columnData.Width = 357;
            // 
            // btnRefreshPorts
            // 
            this.btnRefreshPorts.Image = global::FirstWinApp.Properties.Resources.refresh;
            this.btnRefreshPorts.Location = new System.Drawing.Point(234, 14);
            this.btnRefreshPorts.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRefreshPorts.Name = "btnRefreshPorts";
            this.btnRefreshPorts.Size = new System.Drawing.Size(28, 24);
            this.btnRefreshPorts.TabIndex = 11;
            this.btnRefreshPorts.UseVisualStyleBackColor = true;
            this.btnRefreshPorts.Click += new System.EventHandler(this.btnRefreshPorts_Click);
            // 
            // fmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(687, 459);
            this.Controls.Add(this.radioBtnBin);
            this.Controls.Add(this.btnRefreshPorts);
            this.Controls.Add(this.radioBtnHex);
            this.Controls.Add(this.labelHowToDo);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.gvData);
            this.Controls.Add(this.cmbBoxStopBits);
            this.Controls.Add(this.labelPort);
            this.Controls.Add(this.cmbBoxParity);
            this.Controls.Add(this.labelBaudrate);
            this.Controls.Add(this.cmbBoxDataBit);
            this.Controls.Add(this.labelDataBit);
            this.Controls.Add(this.cmbBoxBaudrate);
            this.Controls.Add(this.labelParity);
            this.Controls.Add(this.cmbBoxPorts);
            this.Controls.Add(this.labelStopBits);
            this.Controls.Add(this.btnStartAndStop);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.Name = "fmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.fmMain_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gvData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.Label labelStopBits;
        private System.Windows.Forms.Label labelParity;
        private System.Windows.Forms.Label labelDataBit;
        private System.Windows.Forms.Label labelBaudrate;
        private System.Windows.Forms.Button btnStartAndStop;
        private System.Windows.Forms.ComboBox cmbBoxPorts;
        private System.Windows.Forms.ComboBox cmbBoxStopBits;
        private System.Windows.Forms.ComboBox cmbBoxParity;
        private System.Windows.Forms.ComboBox cmbBoxDataBit;
        private System.Windows.Forms.ComboBox cmbBoxBaudrate;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.DataGridView gvData;
        private System.Windows.Forms.RadioButton radioBtnBin;
        private System.Windows.Forms.RadioButton radioBtnHex;
        private System.Windows.Forms.Label labelHowToDo;
        private System.Windows.Forms.Button btnRefreshPorts;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnData;
    }
}

