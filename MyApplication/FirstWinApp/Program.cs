﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FirstWinApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            MessageFirst();
            Application.Run(new fmMain());
        }

        private static void MessageFirst()
        {
            MessageBox.Show(
                "Программа «Анализатор COM-порта» приветствует Вас!\n\nРазработчик:\nСтудент группы ПЭ1-18 Новиков П. Б.\nСтарший преподаватель Каткова А. А.\n\nСФ МЭИ, ЭиМТ, 2021",
                "Программа «Анализатор COM-порта»",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1);
        }
    }
}
