﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FirstWinApp
{
    public partial class fmMain : Form
    {        
        public fmMain()
        {
            InitializeComponent();
            FillControls();
            AddListItemDelegate = new AddListItem(AddDataRow);
        }

        public SerialPort SerialPort;

        /// <summary>
        /// Метод для заполнения контролов с настройками
        /// </summary>
        private void FillControls()
        {
            try
            {
                // Обновляем список портов
                RefreshPorts();

                //Добавляем прочие настройки
                string[] baudrate = { "2400", "4800", "9600", "14400", "19200", "38400" };
                string[] data = { "5", "6", "7", "8" };
                string[] parity = { "No", "Odd", "Even" };
                string[] stopbit = { "1", "2" };

                cmbBoxBaudrate.Items.Clear();
                cmbBoxBaudrate.Items.AddRange(baudrate);
                cmbBoxDataBit.Items.Clear();
                cmbBoxDataBit.Items.AddRange(data);
                cmbBoxParity.Items.Clear();
                cmbBoxParity.Items.AddRange(parity);
                cmbBoxStopBits.Items.Clear();
                cmbBoxStopBits.Items.AddRange(stopbit);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Обновление списка портов
        /// </summary>
        private void RefreshPorts()
        {
            try
            {
                string[] ports = SerialPort.GetPortNames();
                cmbBoxPorts.Items.Clear();
                cmbBoxPorts.Items.AddRange(ports);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Флаг получения данных
        /// </summary>
        public bool IsStarted = false;

        /// <summary>
        /// Выставляем возможность редактирования настроек
        /// </summary>
        private void EnableControls()
        {
            try
            {
                cmbBoxPorts.Enabled = !IsStarted;
                cmbBoxBaudrate.Enabled = !IsStarted;
                cmbBoxDataBit.Enabled = !IsStarted;
                cmbBoxParity.Enabled = !IsStarted;
                cmbBoxStopBits.Enabled = !IsStarted;
                btnRefreshPorts.Enabled = !IsStarted;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Старт работы
        /// </summary>
        private void StartWork()
        {
            try
            {
                // Если порт открыт, заканчиваем работу               
                if (SerialPort!= null && SerialPort.IsOpen)
                    StopWork();                

                // Создаем экземпляр порта                
                SerialPort = new SerialPort();                
                // Добавляем ему название                
                SerialPort.PortName = cmbBoxPorts.Text;                
                // Добавляем Baudrate                
                SerialPort.BaudRate = Int32.Parse(cmbBoxBaudrate.Text);                
                // Добавляем DataBits                
                SerialPort.DataBits = Int32.Parse(cmbBoxDataBit.Text);

                ///Добавляем Parity
                if (cmbBoxParity.Text == "No")
                    SerialPort.Parity = Parity.None;
                else if (cmbBoxParity.Text == "Odd")
                    SerialPort.Parity = Parity.Odd;
                else if (cmbBoxParity.Text == "Even")
                    SerialPort.Parity = Parity.Even;

                ///Добавляем StopBits
                if (cmbBoxStopBits.Text == "1")
                    SerialPort.StopBits = StopBits.One;
                else if (cmbBoxStopBits.Text == "2")
                    SerialPort.StopBits = StopBits.Two;

                SerialPort.Open();
                
                // Подписываемся на событие приема сообщений в порт                
                SerialPort.DataReceived += _SerialPort_DataReceived;                
            }
            catch (Exception ex)
            {
                throw; //эту же ошибку пробрасываем дальше, до следующего catch
            }

            IsStarted = true; //Выставляем в самом конце, т.к. сюда мы дойдем, если не будет ошибок
        }
        
        /// <summary>
        /// Событие приема сообщений в порт
        /// </summary>
        private void _SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                var temporaryArrayForData = new byte[SerialPort.BytesToRead];
                SerialPort.Read(temporaryArrayForData, 0, SerialPort.BytesToRead);

                var item = new DataItem()
                {
                    DateTime = DateTime.Now,
                    Data = temporaryArrayForData,
                };

                this.Invoke(AddListItemDelegate, new Object[] { item });                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }        

        /// <summary>
        /// Стоп работы
        /// </summary>
        private void StopWork()
        {
            try
            {
                if (SerialPort != null)
                {
                    SerialPort.DataReceived -= _SerialPort_DataReceived;
                    if (SerialPort.IsOpen)
                        SerialPort.Close();
                }
            }
            catch (Exception ex)
            {
                throw; //эту же ошибку пробрасываем дальше, до следующего catch
            }

            IsStarted = false; //Выставляем в самом конце, т.к. сюда мы дойдем, если не будет ошибок
        }

        /// <summary>
        /// Обработчик нажатия кнопки Старт и стоп
        /// </summary>
        private void btnStartAndStop_Click(object sender, EventArgs e)
        {
            try 
            {                
                StartAndStopClick();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Метод работы кнопки старт и стоп
        /// </summary>
        private void StartAndStopClick()
        {
            try
            {
                ValidateSettings();

                if (IsStarted)
                {
                    StopWork();                    
                    btnStartAndStop.Text = "Старт";
                    btnStartAndStop.BackColor = Color.SeaGreen;
                }
                else
                {
                    StartWork();                    
                    btnStartAndStop.Text = "Стоп";
                    btnStartAndStop.BackColor = Color.Crimson;
                }
                EnableControls();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Проверка настроек
        /// </summary>
        private void ValidateSettings()
        {
            if (string.IsNullOrEmpty(cmbBoxPorts.Text))
                throw new Exception("Не задан COM-порт!");

            if (string.IsNullOrEmpty(cmbBoxBaudrate.Text))
                throw new Exception("Не задана скорость передачи!");

            if (string.IsNullOrEmpty(cmbBoxDataBit.Text))
                throw new Exception("Не задано количество бит данных!");

            if (string.IsNullOrEmpty(cmbBoxParity.Text))
                throw new Exception("Не заданы настройки четности!");

            if (string.IsNullOrEmpty(cmbBoxStopBits.Text))
                throw new Exception("Не заданы настройки стоповых битов!");
        }
        /// <summary>
        /// Обработка нажатия кнопки обновления списка доступных портов
        /// </summary>
        private void btnRefreshPorts_Click(object sender, EventArgs e)
        {
            try
            {
                RefreshPorts();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public delegate void AddListItem(DataItem itemToAdd);
        public AddListItem AddListItemDelegate;

        /// <summary>
        /// Добавление строки
        /// </summary>
        public void AddDataRow(DataItem itemToAdd)
        {      
            try
            {
                var dataCount = itemToAdd.Data.Count();
                var dataTwo = new string[dataCount];
                var timeStr = itemToAdd.DateTime.ToString("dd.MM.yyyy HH:mm:ss.ff");
                var dataStr = ""; 

                if (radioBtnBin.Checked == true)
                {
                    for (int i = 0; i < dataCount; i++)
                    {
                        dataTwo[i] = Convert.ToString(itemToAdd.Data[i], 2).PadLeft(8, '0');
                    }
                    dataStr = String.Join("-", dataTwo);                    
                }
                else
                {
                    dataStr = BitConverter.ToString(itemToAdd.Data);                    
                }

                gvData.Rows.Add(timeStr, dataStr);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Удаление строк
        /// </summary>
        public void ClearDataRows()
        {
            try
            {
                gvData.Rows.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Обработка нажатия кнопки "Очистить"
        /// </summary>
        private void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearDataRows();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Событие перед закрытием формы (Останавливает работу порта)
        /// </summary>
        private void fmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                StopWork();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
