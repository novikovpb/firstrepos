﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstWinApp
{
    /// <summary>
    /// Класс для входных сообщений
    /// </summary>
    public class DataItem
    {
        public DateTime DateTime;
        public byte[] Data;
    }
}
